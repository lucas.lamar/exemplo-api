import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Ong extends Model {
    static init(sequelize) {
        super.init(
            {
                name: Sequelize.STRING,
                cpf: Sequelize.STRING,
                email: Sequelize.STRING,
                telefone: Sequelize.STRING,
                cidade: Sequelize.STRING,
                uf: Sequelize.STRING,
                password: Sequelize.VIRTUAL,
                password_hash: Sequelize.STRING,
            },
            { sequelize, timestamps: true, underscored: false }
        );
        this.addHook('beforeSave', async (ongInfo) => {
            if (ongInfo.password) {
                ongInfo.password_hash = await bcrypt.hash(ongInfo.password, 8);
            }
        });
        return this;
    }

    static associate(models) {
        this.hasMany(models.Case, { foreignKey: 'ong_id', as: 'ong' });
    }

    async checkPassword(password) {
        return bcrypt.compare(password, this.password_hash);
    }
}
export default Ong;
