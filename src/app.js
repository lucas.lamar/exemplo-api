import './config/moduleAliasConfig';
import 'dotenv/config';
import express from 'express';
import 'express-async-errors';
import routes from './app/config/routes';
import handleErrors from './app/middlewares/handleErrors';
import './databases/models';

class App {
    constructor() {
        this.server = express();

        this.middlewares();
        this.routes();
        this.server.use(handleErrors);
    }

    middlewares() {
        this.server.use(express.json());
    }

    routes() {
        this.server.use(routes);
    }
}

export default new App().server;
