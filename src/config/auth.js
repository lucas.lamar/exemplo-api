export default {
    secret: process.env.SESSION_SECRET,
    expiresIn: '1d',
};
