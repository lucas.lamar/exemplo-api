import CaseRepository from '@/app/repositories/CaseRepository';

import CreateService from './create';
import UpdateService from './update';
import DeleteService from './delete';

const getAllCases = (query, ongId) => CaseRepository.all({ query, ongId });
const createCase = async (body, ongId) => CreateService.create(body, ongId);
const updateCase = async (id, ongId, body) =>
    UpdateService.update(id, ongId, body);
const deleteCase = async (id) => DeleteService.delete(id);

export default {
    getAllCases,
    createCase,
    updateCase,
    deleteCase,
};
