import CaseRepository from '@/app/repositories/CaseRepository';
import { BadRequest } from '@/app/config/errors';

class Delete {
    async delete(id) {
        try {
            const cases = await CaseRepository.delete(id);
            if (!cases) {
                throw new BadRequest('cases não existe!');
            }
        } catch (error) {
            throw new BadRequest(error);
        }
    }
}

export default new Delete();
