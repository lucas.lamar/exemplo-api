import CaseRepository from '@/app/repositories/CaseRepository';
// import { BadRequest } from '@/app/config/errors';

class Update {
    async update(id, ongId, caseInfo) {
        caseInfo.ong_id = ongId;
        // const caseExists = await CaseRepository.findOne(id);
        // if (!caseExists) {
        //     throw new BadRequest('Ong não existe!');
        // }

        const [caseUpdate] = await CaseRepository.update(id, caseInfo);

        // delete caseUpdate.id;
        return caseUpdate;
    }
}

export default new Update();
