import CaseRepository from '@/app/repositories/CaseRepository';
import { BadRequest } from '@/app/config/errors';

class Create {
    async create(caseInfo, ongId) {
        try {
            caseInfo.ong_id = ongId;

            return CaseRepository.create(caseInfo);
        } catch (error) {
            throw new BadRequest(error);
        }
    }
}

export default new Create();
