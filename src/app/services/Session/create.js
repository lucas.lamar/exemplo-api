import OngRepository from '@/app/repositories/OngRepository';
import { BadRequest } from '@/app/config/errors';

import jwt from 'jsonwebtoken';
import authConfig from '@/config/auth';

class Create {
    async create(session) {
        try {
            const ongExists = await OngRepository.findOne(false, session);
            if (!ongExists) {
                throw new BadRequest('Ong não existe!');
            }

            if (
                session.password &&
                !(await ongExists.checkPassword(session.password))
            ) {
                throw new BadRequest('Email/Password estão incorretos!');
            }
            return {
                token: jwt.sign({ id: ongExists.id }, authConfig.secret, {
                    expiresIn: authConfig.expiresIn,
                }),
                ongId: ongExists.id,
                name: ongExists.name,
                email: ongExists.email,
            };
        } catch (error) {
            throw new BadRequest(error);
        }
    }
}

export default new Create();
