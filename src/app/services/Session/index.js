import CreateService from './create';

const createSession = async (body) => CreateService.create(body);

export default {
    createSession,
};
