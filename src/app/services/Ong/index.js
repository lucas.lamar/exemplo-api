import OngRepository from '@/app/repositories/OngRepository';

import CreateService from './create';
import UpdateService from './update';
import DeleteService from './delete';

const getAllOngs = (query) => OngRepository.all(query);
const createOng = async (body) => CreateService.create(body);
const updateOng = async (id, body) => UpdateService.update(id, body);
const deleteOng = async (id) => DeleteService.delete(id);

export default {
    getAllOngs,
    createOng,
    updateOng,
    deleteOng,
};
