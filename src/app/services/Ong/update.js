import OngRepository from '@/app/repositories/OngRepository';
import { BadRequest } from '@/app/config/errors';

class Update {
    async update(id, ong) {
        const ongExists = await OngRepository.findOne(id);

        if (!ongExists) {
            throw new BadRequest('Ong não existe!');
        }

        if (
            ong.oldPassword &&
            !(await ongExists.checkPassword(ong.oldPassword))
        ) {
            throw new BadRequest('Antiga senha errada!');
        }
        const [ongUpdate] = await OngRepository.update(id, ong);

        // delete ongUpdate.id;
        return ongUpdate;
    }
}

export default new Update();
