import OngRepository from '@/app/repositories/OngRepository';
import { BadRequest } from '@/app/config/errors';

class Delete {
    async delete(id) {
        try {
            const ong = await OngRepository.delete(id);
            if (!ong) {
                throw new BadRequest('ERROR');
            }
        } catch (error) {
            throw new BadRequest(error);
        }
    }
}

export default new Delete();
