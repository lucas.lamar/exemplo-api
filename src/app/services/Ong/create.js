import OngRepository from '@/app/repositories/OngRepository';
import { BadRequest } from '@/app/config/errors';

class Create {
    async create(ong) {
        try {
            return OngRepository.create(ong);
        } catch (error) {
            throw new BadRequest(error);
        }
    }
}

export default new Create();
