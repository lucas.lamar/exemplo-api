import { Op } from 'sequelize';
import Ong from '@/databases/models/Ong';

class OngRepository {
    async all({ page = 1 }) {
        const where = {};
        where[Op.and] = [];

        const query = {
            where,
            order: [['createdAt', 'DESC']],
            distinct: true,
        };

        const limit = 10;
        const { rows: items, count: total } = await Ong.findAndCountAll(
            this.paginate(query, page, limit)
        );

        return { items, total };
    }

    paginate(query, page, limit) {
        return {
            ...query,
            limit,
            offset: (page - 1) * limit,
        };
    }

    async findOne(id, ongInfo = {}) {
        const where = {};
        where[Op.and] = [];

        if (id) {
            where[Op.and].push({ id });
        }

        if (ongInfo.cpf) {
            where[Op.and].push({ cpf: ongInfo.cpf });
        }

        if (ongInfo.email) {
            where[Op.and].push({ email: ongInfo.email });
        }

        return Ong.findOne({ where });
    }

    async checkPassword(ongInfo) {
        return Ong.checkPassword(ongInfo);
    }

    async create(ongInfo) {
        const ong = await Ong.create(ongInfo);
        return ong;
    }

    async update(id, ongInfo) {
        const [, ong] = await Ong.update(ongInfo, {
            where: { id },
            individualHooks: true,
            returning: true,
        });

        return ong;
    }

    delete(id) {
        return Ong.destroy({
            where: { id },
        });
    }
}

export default new OngRepository();
