import { Op } from 'sequelize';
import Case from '@/databases/models/Case';

class CaseRepository {
    async all({ page = 1, ongId }) {
        const where = {};
        where[Op.and] = [];

        if (ongId) {
            where[Op.and].push({ ong_id: ongId });
        }

        const query = {
            where,
            order: [['createdAt', 'DESC']],
            distinct: true,
        };

        const limit = 10;
        const { rows: items, count: total } = await Case.findAndCountAll(
            this.paginate(query, page, limit)
        );

        return { items, total };
    }

    paginate(query, page, limit) {
        return {
            ...query,
            limit,
            offset: (page - 1) * limit,
        };
    }

    async create(caseInfo) {
        const cases = await Case.create(caseInfo);
        return cases;
    }

    async update(id, caseInfo) {
        const [, cases] = await Case.update(caseInfo, {
            where: { id },
            individualHooks: true,
            returning: true,
        });

        return cases;
    }

    delete(id) {
        return Case.destroy({
            where: { id },
        });
    }
}

export default new CaseRepository();
