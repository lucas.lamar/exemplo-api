import yup from '../yupLocale';

export default async (req, res, next) => {
    try {
        const schema = yup.object().shape({
            password: yup
                .string()
                .required('Password is required')
                .min(8)
                .matches(
                    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/gm
                ),
            passwordConfirmation: yup
                .string()
                .oneOf([yup.ref('password'), null], 'Passwords must match')
                .required(),
        });
        await schema.validate(req.body, { abortEarly: false });
        return next();
    } catch (error) {
        return res
            .status(400)
            .json({ validation: 'Validation fails.', message: error });
    }
};
