import yup from '../yupLocale';

export default async (req, res, next) => {
    try {
        const schema = yup.object().shape({
            name: yup
                .string()
                .min(3)
                .matches(/^[a-záàâãéèêíïóôõöúçñ ]+$/i)
                .required('Nome é obrigatório!'),
            cpf: yup.string().required('cpf é obrigatório!'),
            cidade: yup
                .string()
                .matches(/^[a-záàâãéèêíïóôõöúçñ ]+$/i)
                .required('Cidade é obrigatório!'),
            uf: yup
                .string()
                .min(2)
                .max(2)
                .matches(/^[a-záàâãéèêíïóôõöúçñ ]+$/i)
                .required('Uf é obrigatório!'),
            email: yup.string().email().required('E-mail é obrigatório!'),
            telefone: yup
                .string()
                .matches(/^[1-9][1-9]9([0-9]{4}){2}$/gm)
                .required('Telefone é obrigatório!'),
            oldPassword: yup
                .string()
                .min(8)
                .matches(
                    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/gm
                ),
            password: yup
                .string()
                .min(8)
                .when('oldPassword', (oldPassword, field) =>
                    oldPassword ? field.required() : field
                )
                .matches(
                    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}$/gm
                ),
            confirmPassword: yup
                .string()
                .when('password', (password, field) =>
                    password
                        ? field.required().oneOf([yup.ref('password')])
                        : field
                ),
        });
        await schema.validate(req.body, { abortEarly: false });
        return next();
    } catch (error) {
        return res
            .status(400)
            .json({ error: 'Validation fails.', messages: error.inner });
    }
};
