import CaseService from '@/app/services/Case';

class CaseController {
    async index(req, res) {
        const { page = 1 } = req.query;
        const { ongId } = req;
        const { items, total } = await CaseService.getAllCases(
            req.query,
            ongId
        );
        return res.json({ items, total, page: parseInt(page, 10) });
    }

    async create(req, res) {
        const { ongId } = req;
        const ong = await CaseService.createCase(req.body, ongId);
        return res.status(201).json(ong);
    }

    async update(req, res) {
        const { id, ongId } = req.params;
        const ong = await CaseService.updateCase(id, ongId, req.body);
        return res.status(200).json(ong);
    }

    async delete(req, res) {
        const { id } = req.params;
        await CaseService.deleteCase(id);
        return res.status(204).send();
    }
}

export default new CaseController();
