import OngService from '@/app/services/Ong';

class OngController {
    async index(req, res) {
        const { page = 1 } = req.query;
        const { items, total } = await OngService.getAllOngs(req.query);
        return res.json({ items, total, page: parseInt(page, 10) });
    }

    async create(req, res) {
        const ong = await OngService.createOng(req.body);
        return res.status(201).json(ong);
    }

    async update(req, res) {
        const { id } = req.params;
        const ong = await OngService.updateOng(id, req.body);
        return res.status(200).json(ong);
    }

    async delete(req, res) {
        const { id } = req.params;
        await OngService.deleteOng(id);
        return res.status(204).send();
    }
}

export default new OngController();
