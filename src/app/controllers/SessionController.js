import SessionService from '@/app/services/Session';

class SessionController {
    async create(req, res) {
        const session = await SessionService.createSession(req.body);
        return res.status(200).json(session);
    }
}

export default new SessionController();
