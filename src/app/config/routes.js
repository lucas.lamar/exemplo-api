import { Router } from 'express';

import OngController from '@/app/controllers/OngController';
import SessionController from '@/app/controllers/SessionController';
import CaseController from '@/app/controllers/CaseController';

import AuthMiddleware from '@/app/middlewares/auth';
import ValidadorCpf from '@/app/utils/validators/Global/validadorCpf';
import ConfirmPassword from '@/app/utils/validators/Global/confirmPassword';

import NewOng from '@/app/utils/validators/Ong/new';
import UpdateOng from '@/app/utils/validators/Ong/update';
import ValidateCase from '@/app/utils/validators/Cases/cases';

const routes = new Router();

routes.post('/api/v1/session', SessionController.create);

routes.get('/api/v1/ongs', [AuthMiddleware], OngController.index);
routes.post(
    '/api/v1/ong',
    [ConfirmPassword, ValidadorCpf, NewOng],
    OngController.create
);
routes.put(
    '/api/v1/ong/:id',
    [UpdateOng, ValidadorCpf, AuthMiddleware],
    OngController.update
);

routes.get('/api/v1/cases', [AuthMiddleware], CaseController.index);
routes.post(
    '/api/v1/case',
    [AuthMiddleware, ValidateCase],
    CaseController.create
);
routes.put(
    '/api/v1/case/:id',
    [AuthMiddleware, ValidateCase],
    CaseController.update
);
routes.delete('/api/v1/case/:id', [AuthMiddleware], CaseController.delete);

export default routes;
